package com.test.calcuator.service;

import com.test.calcuator.service.exception.ExpressionFormatException;
import com.test.calcuator.service.validate.ExpressionValidator;

import java.util.regex.Pattern;

public class DecimalExpression implements FormatExpression {

    public static final Pattern DIGITS = Pattern.compile("[0-9]+");
    public static final Pattern OPERATORS = Pattern.compile("[*\\-/+]+");

    public String getDecimalTemplate(String expression) throws ExpressionFormatException {
        ExpressionValidator.validateRequestAndThrowExceptionIfExistWrongOperators(expression, DIGITS);//throws exception if expression is wrong
        return expression;
    }

    @Override
    public boolean isInstanceOfExpression(String expression) {
        try {
            ExpressionValidator.validateRequestAndThrowExceptionIfExistWrongOperators(expression, DIGITS);
        } catch (ExpressionFormatException e){
            return false;
        }
        return true;
    }
}
