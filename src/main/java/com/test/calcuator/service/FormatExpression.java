package com.test.calcuator.service;

import com.test.calcuator.service.exception.ExpressionFormatException;

import java.util.regex.Pattern;

public interface FormatExpression {
    String getDecimalTemplate(String expression) throws ExpressionFormatException;
    boolean isInstanceOfExpression(String expression);
}
