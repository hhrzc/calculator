package com.test.calcuator.service.calculating;

import com.test.calcuator.model.templates.Operators;
import com.test.calcuator.service.exception.ExpressionFormatException;
import com.test.calcuator.service.validate.ExpressionValidator;

import java.util.*;
import java.util.regex.Pattern;

public class PolishCalculating {
    private List<Object> polishLine;
    private Stack<Operators> operatorsLine;
    private final Pattern OPERATORS = Pattern.compile("[+\\-/*]");
    private final Pattern DIGITS = Pattern.compile("[0-9]+\\.[0-9]+|[0-9]+");


    public PolishCalculating(String expression) throws ExpressionFormatException{
        generatePolishLineFromExpression(expression);
    }

    public void generatePolishLineFromExpression(String expression) throws ExpressionFormatException {
        validateRequestAndThrowExceptionIfExistWrongOperators(expression);
        generatePolishLine(expression);
    }


    private void validateRequestAndThrowExceptionIfExistWrongOperators(String generalRequest) throws ExpressionFormatException{
        if (generalRequest.startsWith(OPERATORS.pattern()) && OPERATORS.matcher(generalRequest.substring(1,1)).matches()){
            throw new ExpressionFormatException();
        }
        ExpressionValidator.validateRequestAndThrowExceptionIfExistWrongOperators(generalRequest, DIGITS);
    }

    private void generatePolishLine(String generalRequest) throws ExpressionFormatException{

        generalRequest = generalRequest.replaceAll(" ", "");
        String[] digitsLine = Arrays.stream(generalRequest.split(OPERATORS.pattern())).filter(s -> !s.isEmpty()).toArray(String[]::new);
        if (OPERATORS.matcher(generalRequest.substring(0,1)).matches()){
            if (generalRequest.startsWith("-")) {
                digitsLine[0] = "-"+digitsLine[0];
                generalRequest = generalRequest.substring(1);
            }
        }
        String[] operatorsLine = Arrays.stream(generalRequest.split(DIGITS.pattern())).filter(s -> !s.isEmpty()).toArray(String[]::new);

        String[] concatenateLine = new String[digitsLine.length + operatorsLine.length];
        List<String> concatenateList = new ArrayList<>();
        int j = 0;
        for (int i = 0; i < digitsLine.length; i++) {
            if (digitsLine.length-1 >= i) {
                concatenateLine[j++] = digitsLine[i];
                concatenateList.add(digitsLine[i]);
            }

            if (operatorsLine.length-1 >= i) {
                concatenateLine[j++] = operatorsLine[i];
                concatenateList.add(operatorsLine[i]);
            }
        }


        this.polishLine = new ArrayList<>();
        this.operatorsLine = new Stack<>();
        for (String s : concatenateList) {
            putIntoPolishLine(s);
        }
        addRemainder();
    }

    public Double getDoubleValue(){
        Stack<Double> temp = new Stack<>();
        Double result;
        for (int i = 0; i < polishLine.size(); i++) {
            if (polishLine.get(i) instanceof Double) {
                temp.add((Double) polishLine.remove(i--));
            } else { //if Operator
                result = DecimalCalc.calc(temp.pop(), temp.pop(), (Operators)polishLine.remove(i--));
                temp.push(result);
            }
        }
        return temp.peek();
    }


    private void addRemainder() {
        int size = operatorsLine.size();
        if (size>0){
            for (int i = 0; i < size; i++) {
                polishLine.add(operatorsLine.pop());
            }
        }
    }

    private void putIntoPolishLine(String s) throws ExpressionFormatException{
        if (isDigit(s)){
            polishLine.add(Double.valueOf(s));
        } else {
            Operators operator = determineOperatorFromString(s);
            int weight = determineOddsByWeightBetweenIncomingAndExistingInStackOperators(operator);
            if (weight > 0) {
                operatorsLine.add(operator);
            } else {
                addRemainder();
                operatorsLine.add(operator);
            }
        }
    }

    private int determineOddsByWeightBetweenIncomingAndExistingInStackOperators(Operators operator) {
        try {
            int odds = operator.getWeight() - operatorsLine.peek().getWeight();
            return odds;
        } catch (EmptyStackException e){
            return 1;
        }
    }

    private Operators determineOperatorFromString(String s) throws ExpressionFormatException{
        switch (s){
            case "+":
                return Operators.PLUS;
            case "-":
                return Operators.MINUS;
            case "/":
                return Operators.DIVIDER;
            case "*":
                return Operators.MULTIPLIER;
            default:
                throw new ExpressionFormatException();
        }
    }

    private boolean isDigit(String s) {
        return DIGITS.matcher(s).matches();
    }


}
