package com.test.calcuator.service.calculating;

import com.test.calcuator.model.templates.Operators;

public class DecimalCalc {
    public static Double calc(Double a, Double b, Operators operator){
        switch (operator){
            case PLUS:
                return add(a, b);
            case MULTIPLIER:
                return mlt(a, b);
            case DIVIDER:
                return div(a, b);
            case MINUS:
                return sub(a, b);
        }
        return 0.0;
    }

    public static Double add(Double a, Double b) {
        return a+b;
    }

    public static Double sub(Double a, Double b) {
        return b-a;
    }

    public static Double mlt(Double a, Double b) {
        return a*b;
    }

    public static double div(Double a, Double b) {
        return b/a;
    }
}
