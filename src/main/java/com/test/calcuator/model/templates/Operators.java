package com.test.calcuator.model.templates;

public enum Operators {
    PLUS("+"), MINUS("-"), DIVIDER("/"), MULTIPLIER("*");

    private String value;
    Operators(String value){
        this.value = value;
    }


    public int getWeight() {
        return (this == PLUS || this == MINUS) ? 1 : 2;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
